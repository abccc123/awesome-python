Python 3 教程
============https://github.com/michaelliao



# Pycharm配置conda环境(解决新版本无法识别可执行文件问题) 
D:\ProgramData\anaconda3\condabin\conda.bat



## 环境安装
```
#https://blog.csdn.net/sinat_21591675/article/details/82770360
mkdir ~/.pip
cat > ~/.pip/pip.conf<<efo
[global]
index-url = https://mirrors.aliyun.com/pypi/simple
[install]
trusted-host = https://pypi.tuna.tsinghua.edu.cn
efo
#Windows更换pip/pip3源
#打开目录：%appdata%
#新增pip文件夹，新建pip.ini文件
#给pip.ini添加内容
#[global]
#timeout = 6000
#index-url = https://pypi.tuna.tsinghua.edu.cn/simple
#trusted-host = pypi.tuna.tsinghua.edu.cn
```
pip3 install requirements.txt
pip3 install -r  requirements.txt

## pycharm pip镜像

[url] https://blog.csdn.net/qq_34173549/article/details/81485489
阿里云 http://mirrors.aliyun.com/pypi/simple
  中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple
  豆瓣(douban) http://pypi.douban.com/simple
  清华大学 https://pypi.tuna.tsinghua.edu.cn/simple
  中国科学技术大学 http://pypi.mirrors.ustc.edu.cn/simple

### Jupyter notebook的代码要想写得规范，推荐用Code prettify插件
pip install jupyter_contrib_nbextensions
pip install yapf
[Python进阶](https://github.com/eastlakeside/interpy-zh)




## PyCharm设置自己的默认模板
2. file- setting..- Editor- Code Style - File and Code Templates - Python Script
需要设置什么内容，现在就可以写入了，相关变量参考：
复制代码
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ${PROJECT_NAME} - 当前Project名称;
# ${NAME} - 在创建文件的对话框中指定的文件名;
# ${USER} - 当前用户名;
# ${DATE} ${TIME} 




## jupyter
```python
jupyter notebook --generate-config
import webbrowser
webbrowser.register(chrome,None, webbrowser.GenericBrowser("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"))
c.NotebookApp.browser ='chrome'
#https://www.bilibili.com/video/BV1K7411c7EL?p=4
```
## PyCharm 使用技巧
常用快捷键
1、编辑（Editing）
Ctrl + Space 基本的代码完成（类、方法、属性）
Ctrl + Alt + Space 快速导入任意类
Ctrl + Shift + Enter 语句完成
Ctrl + P 参数信息（在方法中调用参数）
Ctrl + Q 快速查看文档
Shift + F1 外部文档
Ctrl + 鼠标 简介
Ctrl + F1 显示错误描述或警告信息
Alt + Insert 自动生成代码
Ctrl + O 重新方法
Ctrl + Alt + T 选中
Ctrl + / 行注释
Ctrl + Shift + / 块注释
Ctrl + W 选中增加的代码块
Ctrl + Shift + W 回到之前状态
Ctrl + Shift + ]/[ 选定代码块结束、开始
Alt + Enter 快速修正
Ctrl + Alt + L 代码格式化
Ctrl + Alt + O 优化导入
Ctrl + Alt + I 自动缩进
Tab / Shift + Tab 缩进、不缩进当前行
Ctrl+X/Shift+Delete 剪切当前行或选定的代码块到剪贴板
Ctrl+C/Ctrl+Insert 复制当前行或选定的代码块到剪贴板
Ctrl+V/Shift+Insert 从剪贴板粘贴
Ctrl + Shift + V 从最近的缓冲区粘贴
Ctrl + D 复制选定的区域或行
Ctrl + Y 删除选定的行
Ctrl + Shift + J 添加智能线
Ctrl + Enter 智能线切割
Shift + Enter 另起一行
Ctrl + Shift + U 在选定的区域或代码块间切换
Ctrl + Delete 删除到字符结束
Ctrl + Backspace 删除到字符开始
Ctrl + Numpad+/- 展开折叠代码块
Ctrl + Numpad+ 全部展开
Ctrl + Numpad- 全部折叠
Ctrl + F4 关闭运行的选项卡

2、查找/替换(Search/Replace)

F3 下一个
Shift + F3 前一个
Ctrl + R 替换
Ctrl + Shift + F 全局查找
Ctrl + Shift + R 全局替换


3、运行(Running)

Alt + Shift + F10 运行模式配置
Alt + Shift + F9 调试模式配置
Shift + F10 运行
Shift + F9 调试
Ctrl + Shift + F10 运行编辑器配置
Ctrl + Alt + R 运行manage.py任务


4、调试(Debugging)

　　F8 跳过
　　F7 进入
　　Shift + F8 退出
　　Alt + F9 运行游标
　　Alt + F8 验证表达式
　　Ctrl + Alt + F8 快速验证表达式
　　F9 恢复程序
　　Ctrl + F8 断点开关
　　Ctrl + Shift + F8 查看断点

5、导航(Navigation)
Ctrl + N 跳转到类
Ctrl + Shift + N 跳转到符号
Alt + Right/Left 跳转到下一个、前一个编辑的选项卡
F12 回到先前的工具窗口
Esc 从工具窗口回到编辑窗口
Shift + Esc 隐藏运行的、最近运行的窗口
Ctrl + Shift + F4 关闭主动运行的选项卡
Ctrl + G 查看当前行号、字符号
Ctrl + E 当前文件弹出
Ctrl+Alt+Left/Right 后退、前进
Ctrl+Shift+Backspace 导航到最近编辑区域
Alt + F1 查找当前文件或标识
Ctrl+B / Ctrl+Click 跳转到声明
Ctrl + Alt + B 跳转到实现
Ctrl + Shift + I查看快速定义
Ctrl + Shift + B跳转到类型声明
Ctrl + U跳转到父方法、父类
Alt + Up/Down跳转到上一个、下一个方法
Ctrl + ]/[跳转到代码块结束、开始
Ctrl + F12弹出文件结构
Ctrl + H类型层次结构
Ctrl + Shift + H方法层次结构
Ctrl + Alt + H调用层次结构
F2 / Shift + F2下一条、前一条高亮的错误
F4 / Ctrl + Enter编辑资源、查看资源
Alt + Home显示导航条F11书签开关
Ctrl + Shift + F11书签助记开关
Ctrl + #[0-9]跳转到标识的书签
Shift + F11显示书签

6、搜索相关(Usage Search)
Alt + F7/Ctrl + F7文件中查询用法
Ctrl + Shift + F7文件中用法高亮显示
Ctrl + Alt + F7显示用法

7、重构(Refactoring)
F5复制F6剪切
Alt + Delete安全删除
Shift + F6重命名
Ctrl + F6更改签名
Ctrl + Alt + N内联
Ctrl + Alt + M提取方法
Ctrl + Alt + V提取属性
Ctrl + Alt + F提取字段
Ctrl + Alt + C提取常量
Ctrl + Alt + P提取参数

8、控制VCS/Local History
Ctrl + K提交项目
Ctrl + T更新项目
Alt + Shift + C查看最近的变化
Alt + BackQuote(’)VCS快速弹出

9、模版(Live Templates)
Ctrl + Alt + J当前行使用模版
Ctrl +Ｊ插入模版

10、基本(General)
Alt + #[0-9]打开相应的工具窗口
Ctrl + Alt + Y同步
Ctrl + Shift + F12最大化编辑开关
Alt + Shift + F添加到最喜欢
Alt + Shift + I根据配置检查当前文件
Ctrl + BackQuote(’)快速切换当前计划
Ctrl + Alt + S　打开设置页
Ctrl + Shift + A查找编辑器里所有的动作
Ctrl + Tab在窗口间进行切换

一些常用设置：
1. pycharm默认是自动保存的，习惯自己按ctrl + s 的可以进行如下设置：
1. file -> Setting -> General -> Synchronization -> Save files on frame deactivation 和 Save files automatically if application is idle for .. sec 的勾去掉
2. file ->Setting -> Editor -> Editor Tabs -> Mark modified tabs with asterisk 打上勾
2. Alt + Enter: 自动添加包

3. 对于常用的快捷键，可以设置为visual studio(eclipse...)一样的：
file -> Setting -> Keymap -> Keymaps -> vuisual studio -> Apply

4. Pycharm中默认是不能用Ctrl+滚轮改变字体大小的，可以在file -> Setting ->Editor-〉Mouse中设置

5. 要设置Pycharm的字体，要先在file -> Setting ->Editor-〉Editor中选择一种风格并保存，然后才可以改变

6. 在setting中搜索theme可以改变主题，所有配色统一改变

回到顶部(go to top)
一、代码排版，自动PEP8
　　pep8 是Python 语言的一个代码编写规范。如若你是新手，目前只想快速掌握基础，而不想过多去注重代码的的编写风格（虽然这很重要），那你可以尝试一下这个工具 -autopep8。

可参考博客 https://www.cnblogs.com/xxtalhr/p/10645992.html
 
 
## 引用地址

git@github.com:talkpython/100daysofcode-with-python-course.git
https://github.com/jackfrued/Python-100-Days
https://github.com/MLEveryday/100-Days-Of-ML-Code
https://github.com/imtiantian/Python_Awesome_Interview
https://github.com/wkunzhi/Python3-Spider
https://github.com/ityouknow/python-100-days
[代码pdf](https://static.bookstack.cn/projects/liaoxuefeng-python30/books/1531433147.pdf)
