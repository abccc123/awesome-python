# phoneSpider
Scrapy phone information from baidu research using scrapy-splash, this is a simple version.
https://www.cnblogs.com/jclian91/p/8590617.html

## scrapy-splash
pip3 install scrapy-splash

 2. scrapy-splash使用的是Splash HTTP API， 所以需要一个splash instance，一般采用docker运行splash，所以需要安装docker。不同系统的安装命令会不同，如笔者的CentOS7系统的安装方式为：

sudo yum install docker

安装完docker后，可以输入命令‘docker -v’来验证docker是否安装成功。

  3. 开启docker服务，拉取splash镜像（pull the image）：

sudo service docker start
sudo dock pull scrapinghub/splash

运行结果如下：

  4. 开启容器（start the container）：

sudo docker run -p 8050:8050 scrapinghub/splash

此时Splash以运行在本地服务器的端口8050(http).在浏览器中输入'localhost:8050'， 页面如下：

在这个网页中我们能够运行Lua scripts，这对我们在scrapy-splash中使用Lua scripts是非常有帮助的。以上就是我们安装scrapy-splash的全部。
scrapy-splash的实例

  在安装完scrapy-splash之后，不趁机介绍一个实例，实在是说不过去的，我们将在此介绍一个简单的实例，那就是利用百度查询手机号码信息。比如，我们在百度输入框中输入手机号码‘159********’，然后查询，得到如下信息：

我们将利用scrapy-splash模拟以上操作并获取手机号码信息。

  1. 创建scrapy项目phone
  2. 配置settings.py文件，配置的内容如下：

ROBOTSTXT_OBEY = False

SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
}

DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810
}

SPLASH_URL = 'http://localhost:8050'

DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'

具体的配置说明可以参考：  https://pypi.python.org/pypi/scrapy-splash .

  3. 创建爬虫文件phoneSpider.py, 代码如下：

# -*- coding: utf-8 -*-
from scrapy import Spider, Request
from scrapy_splash import SplashRequest

# splash lua script
script = """
         function main(splash, args)
             assert(splash:go(args.url))
             assert(splash:wait(args.wait))
             js = string.format("document.querySelector('#kw').value=%s;document.querySelector('#su').click()", args.phone)
             splash:evaljs(js)
             assert(splash:wait(args.wait))
             return splash:html()
         end
         """

class phoneSpider(Spider):
    name = 'phone'
    allowed_domains = ['www.baidu.com']
    url = 'https://www.baidu.com'
    
    # start request
    def start_requests(self):
        yield SplashRequest(self.url, callback=self.parse, endpoint='execute', args={'lua_source': script, 'phone':'159*******', 'wait': 5})
   
    # parse the html content 
    def parse(self, response):
        info = response.css('div.op_mobilephone_r.c-gap-bottom-small').xpath('span/text()').extract()
        print('='*40)
        print(''.join(info))
        print('='*40)

  4. 运行爬虫，scrapy crawl phone, 结果如下：
 https://github.com/percent4/phoneSpider .当然，有什么问题，也可以载下面留言评论哦~~