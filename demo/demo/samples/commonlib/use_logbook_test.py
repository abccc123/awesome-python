#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# demo - 当前Project名称;
# use_logbook_test - 在创建文件的对话框中指定的文件名;
# chive - 当前用户名;
# 2019/10/2922:49 - 当前系统日期;
# 554961776@qq.com

import os
from use_logbook import run_log as logger

if __name__ == '__main__':
    logger.info("测试log模块，暂时就优化到这一步，后续再改进")