#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# awesome-python - 当前Project名称;
# use_request - 在创建文件的对话框中指定的文件名;
# chive - 当前用户名; pip install requests
# 2021/2/16 22:17  https://blog.csdn.net/qq_37616069/article/details/80376776
# https://requests.readthedocs.io/zh_CN/latest/

import  requests

# get
# response = requests.get("http://www.baidu.com/")
# 也可以这么写
# response = requests.request("get", "http://www.baidu.com/")


# 2. 添加 headers 和 查询参数
kw = {'wd': '长城'}
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"}
# params 接收一个字典或者字符串的查询参数，字典类型自动转换为url编码，不需要urlencode()
response = requests.get("http://www.baidu.com/s?", params=kw, headers=headers)
# # 查看响应内容，response.text 返回的是Unicode格式的数据
# print
# response.text
#
# # 查看响应内容，response.content返回的字节流数据
# print
# respones.content
#
# # 查看完整url地址
# print
# response.url
#
# # 查看响应头部字符编码
# print
# response.encoding
#
# # 查看响应码
# print
# response.status_code



## post
# ormdata = {
#     "type": "AUTO",
#     "i": "i love python",
#     "doctype": "json",
#     "xmlVersion": "1.8",
#     "keyfrom": "fanyi.web",
#     "ue": "UTF-8",
#     "action": "FY_BY_ENTER",
#     "typoResult": "true"
# }
#
# url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule&smartresult=ugc&sessionFrom=null"
#
# headers = {
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
#
# response = requests.post(url, data=formdata, headers=headers)
#
# print
# response.text
#
# # 如果是json文件可以直接显示
# print
# response.json()




### # 根据协议类型，选择不同的代理
# proxies = {
#   "http": "http://12.34.56.79:9527",
#   "https": "http://12.34.56.79:9527",
# }
#
# response = requests.get("http://www.baidu.com", proxies = proxies)
# print response.text


### cookie
response = requests.get("http://www.baidu.com/")

# 7. 返回CookieJar对象:
cookiejar = response.cookies

# 8. 将CookieJar转为字典：
cookiedict = requests.utils.dict_from_cookiejar(cookiejar)
print(cookiejar)
print(cookiedict)

## Python之requests模块-大文件分片上传
```python
# https://www.cnblogs.com/zhuosanxun/p/15100588.html

import requests
from requests_toolbelt import MultipartEncoder
import os


def upload_multipart(url, file_path):
    filename = file_path.split("\\")[-1:][0]
    total_size = os.path.getsize(file_path)
    data = MultipartEncoder(
        fields={
            "filename": filename,
            "totalSize": str(total_size),
            "file": (filename, open(file_path, 'rb'), 'application/octet-stream')
        }
    )
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36",
        "Accept": "application/json",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Content-Type": data.content_type
    }
    with requests.post(url, headers=headers, data=data) as response:
        assert response.status_code == 200

import hashlib


def get_md5(path):
    m = hashlib.md5()
    with open(path, 'rb') as f:
        for line in f:
            m.update(line)
    md5code = m.hexdigest()
    return md5code


import requests
from requests_toolbelt import MultipartEncoder
import os
import math


def upload_slice_file(url, file_path):
    chunk_size = 1024*1024*2
    filename = file_path.split("\\")[-1:][0]
    total_size = os.path.getsize(file_path)
    current_chunk = 1
    total_chunk = math.ceil(total_size/chunk_size)

    while current_chunk <= total_chunk:
        start = (current_chunk - 1)*chunk_size
        end = min(total_size, start+chunk_size)
        with open(file_path, 'rb') as f:
            f.seek(start)
            file_chunk_data = f.read(end-start)
        data = MultipartEncoder(
            fields={
                "filename": filename,
                "totalSize": str(total_size),
                "currentChunk": str(current_chunk),
                "totalChunk": str(total_chunk),
                "md5": get_md5(file_path),
                "file": (filename, file_chunk_data, 'application/octet-stream')
            }
        )
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36",
            "Accept": "application/json",
            "Accept-Encoding": "gzip, deflate",
            "Connection": "keep-alive",
            "Content-Type": data.content_type
        }
        with requests.post(url, headers=headers, data=data) as response:
            assert response.status_code == 200

        current_chunk = current_chunk + 1
```

####
- [bibli下载登录](https://github.com/Hsury/BiliDrive/blob/bilibili/BiliDrive/bilibili.py)
- [自定义线程数下载.m3u8，支持多次重试、合并为.mp4文件](https://github.com/anwenzen/M3u8Download/blob/master/M3u8Download.py)
- [python代码集合（文件下载器、pdf合并、极客时间专栏下载、掘金小册下载、新浪微博爬虫](https://github.com/maixiaojie/pythonCollection)
   - [](https://github.com/maixiaojie/pythonCollection/blob/master/01_downloader.py)
- [多线程上传下载](https://github.com/Aruelius/wenshushu/blob/master/wss.py)
- [email下载器，将邮件以eml文件格式备份到本地](https://github.com/ywdblog/emaildownloader)
- [python下载道客巴巴文件并自动合并为pdf](https://github.com/UnlightedOtaku/doc88Download)
- [介绍如何流式下载大文件，并实现断点续传功能](https://github.com/wangy8961/download-large-files)
- [](https://github.com/aliyun/aliyun-oss-python-sdk/blob/master/examples/upload.py)
