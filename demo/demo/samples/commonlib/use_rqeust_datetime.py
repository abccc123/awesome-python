import requests
import json
from datetime import datetime



# 记录开始时间
start_time = datetime.now()
url = "http://192.168.21.143/api/services/Org/UserLoginIntegrationByUserLoginName"

payload = json.dumps({
  "loginName": "admin",
  "ipAddress": "114.86.227.3",
  "integrationKey": "f82e094f-642e-5af7-35d5-d3525ee4fc3c"
})
headers = {
  'accept': 'application/json',
  'Content-Type': 'application/json-patch+json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

# 记录结束时间
end_time = datetime.now()
# 计算响应时间
response_time = end_time - start_time
# 将时间间隔转换为秒
response_time_in_seconds = response_time.total_seconds()
print(f"响应时间: {response_time_in_seconds:.4f} 秒")