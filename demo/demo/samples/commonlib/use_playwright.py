import re
from playwright.sync_api import sync_playwright
# https://playwright.dev/python/docs/intro
# pip install playwright
# python -m playwright install

# conda config --add channels conda-forge
# conda config --add channels microsoft
# conda install pytest-playwright

def run(playwright):
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto('https://www.topgoer.com/开发环境/go的安装.html')
    # 指定的类名
    class_name = 'search-noresults'  # 替换为你要查找的类名
    # 使用类选择器获取所有具有指定类名的元素
    elements = page.query_selector_all(f'.{class_name}')

    # 打印元素的文本内容
    for i, element in enumerate(elements):
        print(f"Element {i + 1} with class '{class_name}' has text: {element.inner_html()}")

    # # 要筛选的正则表达式模式
    # pattern = re.compile(r'特定内容')
    #
    # # 遍历所有 <p> 元素，筛选包含特定模式的元素
    # for i, p in enumerate(p_elements):
    #     p_text = p.text_content()
    #     if pattern.search(p_text):
    #         print(f"The text content of paragraph {i+1} containing the pattern is: {p_text}")

    browser.close()

with sync_playwright() as playwright:
    run(playwright)
