import ldap
def json_to_ldif(json_data):
    ldif_lines = []
    for key, value in json_data.items():
        if key == "dn":
            ldif_lines.append(key + ": " + value)
        else:
            ldif_lines.append(key + ": " + value)
    return "\n".join(ldif_lines)
def import_json_data_to_ldap(json_data):
    try:
        l = ldap.initialize("ldap://localhost")
        # 绑定用户
        l.simple_bind_s("cn=admin,dc=example,dc=com", "admin_password")
        ldif_data = json_to_ldif(json_data)
        # 导入数据
        l.add_s(json_data["dn"], ldif_data)
        l.unbind_s()
    except ldap.LDAPError as e:
        print("LDAP Error:", e)
if __name__ == "__main__":
    json_user_data = {
        "dn": "uid=user1,ou=People,dc=example,dc=com",
        "objectClass": "inetOrgPerson",
        "uid": "user1",
        "cn": "User One",
        "sn": "One",
        "userPassword": "{SSHA}encrypted_password_here"
    }
    import_json_data_to_ldap(json_user_data)