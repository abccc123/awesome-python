import requests
from html2text import html2text
import chardet
import os
import time
import random
# 假设你有一个名为"urls.txt"的文件，每行包含一个URL
urls_file = r'C:\Users\zhou\Documents\新文件 2.txt'

# 读取文件中的URLs
with open(urls_file, 'r', encoding='utf-8') as file:
    urls = file.readlines()

# with open('your_file.txt', 'rb') as file:
#     content = file.read().decode('gbk', errors='ignore')  # or 'replace'
#     urls = content.splitlines()

# 遍历URLs并处理每个URL
for url in urls:
    url = url.strip()  # 移除URL末尾的换行符
    try:
        # 发送HTTP请求获取HTML内容
        response = requests.get(url)
        response.raise_for_status()  # 确保请求成功
        # 使用chardet检测编码
        encoding = chardet.detect(response.content)['encoding']

        # 使用检测到的编码解码内容
        text = response.content.decode(encoding)
        # 将HTML转换为Markdown
        markdown = html2text(text)

        # 构建Markdown文件名，通常是URL的最后一部分，但需要确保文件名是合法的
        file_name = url.split('/')[-1]
        if '.' not in file_name or file_name.count('.') == len(file_name):
            file_name = 'index.md'  # 如果URL没有合适的部分作为文件名，则使用'index.md'
        else:
            file_name = file_name.split('.')[0] + '.md'
        newpath = os.path.join(r"C:\Users\zhou\Documents\go", file_name)
        print(f"newpath {newpath}")
        # 保存Markdown文件
        with open(newpath, 'w') as md_file:
            md_file.write(markdown)

        time1=random.randint(2, 8)
        print(f"已保存:{file_name}, {time1}")

        # time.sleep(time1)
    except Exception as e:
        print(f"无法下载 {url}: {e}")

print("下载和转换完成。")