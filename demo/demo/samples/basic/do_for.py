#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 打印list:
names = ['Michael', 'Bob', 'Tracy']
for name in names:
    print(name)

# 打印数字 0 - 9
for x in range(10):
    print(x)
print('$$$$'*20)

# 还有for 循环的 用list解析的  https://blog.csdn.net/zl87758539/article/details/51675628

# [对(x)的操作 for x in 集合 if 条件]

# [对(x,y)的操作 for x in 集合1  for y in 集合2 if 条件]
 

# 举一个简单的例子：


x=[1,2,3,4]
y=[5,6,7,8]

# 我想让着两个list中的偶数分别相加，应该结果是2+6,4+6,2+8,4+8

# 下面用一句话来写


[print(a + b) for a in x for b in y if a%2 == 0 and b%2 ==0]