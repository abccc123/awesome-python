




## link
- [wgcloud](https://github.com/tianshiyeben/wgcloud) 服务器集群监控，ES集群监控，CPU监控，内存监控，数据安全监控，服务心跳检测 用微服务springboot+bootstrap实现，为反哺开源社区，二次开源
- [Fabric](http://docs.fabfile.org/en/1.6/)简单来说主要功能就是一个基于Python的服务器批量管理库/工具，Fabric 使用 ssh（通过 paramiko 库）在多个服务器上批量执行任务、上传、下载。在使用Fabric之前，都用Python的paramiko模块来实现需求，相比之后发现Fabric比paramiko模块强大很多。具体的使用方法和说明可以看官方文档介绍。下面写类一个用paramiko（apt-get install python-paramiko）封装的远程操作类的模板
- [Fabricdemo](https://www.cnblogs.com/zhoujinyi/p/6023839.html)
- [Ansible](https://www.cnblogs.com/ee900222/p/ansible.html) 自动化运维的工具，基于Python开发，实现了批量系统配置、批量程序部署、批量运行命令等功能
- [pexpect和paramiko](https://www.imooc.com/article/21641)
  