# 安装Python学习助手

首先确认Python 3.4已安装。

# 下载learning.py

下载地址：

[https://raw.githubusercontent.com/michaelliao/learn-python3/master/teach/learning.py](https://raw.githubusercontent.com/michaelliao/learn-python3/master/teach/learning.py)

(右键 - 另存为)

放到某个目录，例如`C:\Work`下。

# 运行命令：

切换到`learning.py`所在目录，运行：

```
C:\Work> python learning.py
```

如果看到`Ready for Python code on port 39093...`表示运行成功，不要关闭命令行窗口，最小化放到后台运行即可。
https://www.liaoxuefeng.com/wiki/1016959663602400/1017454145929440
![run-learning.py.png](https://raw.githubusercontent.com/michaelliao/learn-python3/master/teach/run-learning.py.png)




## 语法


### 调用函数时使用* **
test(*args)* 的作用其实就是把序列 args 中的每个元素，当作位置参数传进去。比如上面这个代码，如果 args 等于 (1,2,3) ，那么这个代码就等价于 test(1, 2, 3) 。
test(**kwargs)** 的作用则是把字典 kwargs 变成关键字参数传递。比如上面这个代码，如果 kwargs 等于 {'a':1,'b':2,'c':3} ，那这个代码就等价于 test(a=1,b=2,c=3) 



## 目录
├─advance
├─async
├─basic       基本语法 if for input print  dict list set string tuple 字符串操作
              py_conver.py 类型转换 

├─commonlib   第三库 
            use_logbook.py logbook 应用
            user_logging_coloredlogs 日志带颜色
            use_urllib  http请求库【python自带的】
            use_BeautifulSoup_bs4  
├─commonlib\commonlib\use_datetime.py  时间处理
├─context
├─db
    do_sqliteDemo.py do_sqlite.py sqlite 操作
├─debug
    do_logging 日志
├─devops  自动化运维
├─function
├─functional  functools，用于高阶函数：指那些作用于函数或者返回其它函数的函数，Python的functools模块提供了很多有用的功能，其中一个就是偏函数（Partial function）
├─gui
│  └─turtle
├─io
    use_json use_pickle  json对象处理
├─mail
├─module
       sys_cmd 系统命令
├─multitask
├─oop_advance 面向对象高级编程 __slots__ [限制属性] @property
    create_class_on_the_fly.py
    orm.py
    special_call.py
    special_getattr.py
    special_getitem.py
    special_iter.py
    special_str.py
    use_enum.py
    use_metaclass.py  metaclass是创建类
    use_property.py  class get set 属性
    use_slots.py    class  应用 __slots__
├─oop_basic  面向对象编程 【类  多态 继承  实例
├─packages
│  └─pil
├─regex
├─socket
         urllib_demo.py   用法
├─test
    └─appium 手机测试
    └─selenium 浏览器测试
└─web
    └─mvc
        └─templates




## 随机数
```python
import random

random.randint(1,332)
random.random() #0.9876097190672816
```
### 内置的函数
dir() 可以找到模块内定义的所有名称


### builtins内建模块
其实在我们运行python解释器的时候，他会自动导入一些模块，这些函数就是从这些地方来的，这些函数被称为内建函数
dir()
dir(__builtins__)
['__class__', '__contains__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', 'clear', 'copy', 'fromkeys', 'get', 'items', 'keys', 'pop', 'popitem', 'setdefault', 'update', 'values']


### 环境变量
os.environ.get('PATH')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "UploadInterface.settings")  #https://github.com/slzcc/UploadServer/blob/master/manage.py

### 列出当前目录下的所有目录
[x for x in os.listdir('.') if os.path.isdir(x)]

### 怪异语法 if else
 path = path if path else os.path.splitext(self.file_path)[0]

### 怪异语法demo2
```python
   # 分割文件，将分割结果加入下载队列中
        sections = {str(i): (i * section_size, (i + 1) * section_size - 1 if i + 1 != n else end - start) for i in
                    range(n)}
        return sections

        ##正常
        ss=[]
for i in range(n):
    if i + 1 != n:
        ss.append((i * section_size, (i + 1) * section_size - 1))
    else:
        ss.append((i * section_size, end - start))

sections = {str(i):ss}``
```
### 语法简写  if for
- [语法简写](https://blog.csdn.net/weixin_35757704/article/details/90234427)

#### 1 for 简写
先举一个例子：
y = [1,2,3,4,5,6]
[(i*2) for i in y ]
会输出  [2, 4, 6, 8, 10, 12]

#### 1.1 一层for循环简写：
一层 for 循环的简写格式是：（注意有中括号）
[ 对i的操作 for i in 列表 ]

它相当于：
for i in 列表:
    对i的操作

#### 1.2 两层for循环
两层的for循环就是：

[对i的操作 for 单个元素 in 列表 for i in 单个元素]
举个简单的例子：

y_list = ['assss','dvv']
[print(i) for y in y_list for i in y]
得到结果：a s s s s d v v

他类似于：

y_list = ['assss','dvv']
for y in y_list:
    for i in y:
        print(i)
####  if
2 if 简写
格式是：
True的逻辑 if 条件 else False的逻辑

举个例子：
y = 0
x = y+3 if y > 3 else y-1
此时 x = -1
因为 y = 0 ，所以判断 y>3 时执行了 False的逻辑：y-1，所以x的值为 -1

#### 2.1 for 与 if 的结合怎么简写
举个栗子：
x = [1,2,3,4,5,6,7]
[print(i) for i in x if i > 3 ]
它会输出：4 5 6 7
注：使用简写的方式无法对 if 判断为 False 的对象执行操作。

所以它的模板是：
[判断为True的i的操作 for i in 列表 if i的判断 ]


### python中 r'', b'', u'', f'' 的含义
-[](https://blog.csdn.net/qq_35290785/article/details/90634344)
r"" 的作用是去除转义字符.

name = 'processing'
# 以 f开头表示在字符串内支持大括号内的python 表达式
print(f'{name} done in {time.time() - t0:.2f} s') 

字符串前加 b
例: response = b'<h1>Hello World!</h1>'     # b' ' 表示这是一个 bytes 对象

字符串前加 u
例：u"我是含有中文字符组成的字符串。"

作用：
后面字符串以 Unicode 格式 进行编码，一般用在中文字符串前面，防止因为源码储存格式问题，导致再次使用时出现乱码

### 系统
```python
import platform
# https://github.com/rufherg/KanCloud_Downloader/blob/master/kc_download.py
if platform.system() == 'Windows':
    return '1'

```
### 测网速speedtest-cli
 pip install speedtest-cli
 - [](https://github.com/makelove/Python_Master_Courses/blob/87d04641aeb774a40b23dd25825c07afcab74cef/网络管理/测网速speedtest-cli.md#L1)
 
 
 ### 随机生成IPv4地址：
```python
import random
import socket
import struct

ipv4=socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
```
## 假设：有2个数字，现需要从中得到最大的那个数字。如何实现？
```python

    b = 2 
    c = 3
     
    if b > c:
        a = b
    else:
        a = c
#其实还有更多的其它实现方式：

    a = max(b, c)
    a = c > b and c or b
    a = c if c > b else b
    a = [b, c][c > b]

```

## 一些常见的语法糖

    a = 1; b = 2; c = 3
    b, c = c, b
    a < c < b < 5
    '1' * 100
    [1,2,3,4] + [5,6,7,8]

可以看到这些语法，在其它语言里通常不会出现的。但是在Python中却神奇的被支持了，所以这些都是当之无愧的Python语法糖。
切片操作

像列表这类可以支持**切片**操作的对象，则是我最初喜欢Python的一个非常重要的原因。

    l = [1, 2, 3, 4, 5]
    l[2]
    l[:3]
    l[3:]
    l[2:4]
    l[:-1]
    l[:]
    l[::2]

## with语法糖

with语法糖实现的是一个上下文管理器，它主要的特点就是帮助我们自动管理上下文的衔接。即在需要的时候传给我们，不需要的时候自动关闭上下文对象。 需要注意的是：使用with语法糖是有条件的。即其后跟的对象必须要实现__enter__和__exit__这2个魔法属性。具体使用的例子如下：

    with open('example_2.txt', 'r', encoding='utf-8') as f:
     
        for line in f:
     
            print(line, end='')

## for else

    for i in range(1):
        print(i)
        break
    else:
        print('for end')

while else

    i = 1
    while i:
        print(i)
        i -= 1
        break
    else:
        print('while end')

try else

    try:
        1 / 1
    except Exception as e:
        print('except occured')
    else:
        print('it is fine')
    finally:
        print('i am finally')

函数相关语法糖

Python中函数我们都非常的熟悉，而在函数的使用上却有着与其它语言不同的选择。
动态参数

    def example_dynamic_args(*args, **kwargs):
        '''动态参数'''
        print(args)
        print(kwargs)

这个函数的参数与函数相比，其参数会有些不同之处。因为它们在接收参数时使用了不同方式。

    example_dynamic_args(1,'2', True, name='xiaowu', age=18)
    l = [1,'2',False]
    d = {'name': 'xiaoming', age: '16'}
    example_dynamic_args(*l, **d)

匿名函数

匿名函数在很多的语言中都存在，通常在临时需要一个函数的场景下使用。

lambda x: x * 2

Python中使用lambda表达式来实现匿名函数，观察上面的lambda表达式。其特点如下：

        可以接受函数

        函数体只有一个表达式

        无需显式的return语句

        整个表达式在一个语法行内实现

值得注意的是，lambda表达式除了一些语法上的限制之外；其它函数该有的特性它都有。比如：支持动态参数。下面是一个使用lambda表示的场景：

    in_dict = {'a': 10, 'b': 2, 'c': 3}
    print('in_dict:', in_dict)
    out_dict = sorted(in_dict.items(), key=lambda x: x[1])
    print('out_dict', out_dict)

## 列表推导表达式
```python
    in_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7]
    print('array before:', in_list)
    array = [i for i in in_list if i % 2 != 0] # 列表推导表达式
    print('array after:', array)
```
## 生成器推导表达式

    in_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7]
    print('array before:', in_list)
    array = (i for i in in_list if i % 2 != 0) # 生成器推导表达式
    print('array after:', array)

## 集合推导表达式

    in_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7]
    print('array before:', in_list)
    array = {i for i in in_list if i % 2 != 0} # 集合推导表达式
    print('array after:', array)

## 字典推导表达式
    in_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7]
    print('array before:', in_list)
    array = {i: i * 2 for i in in_list if i % 2 != 0}  # 字典推导表达式
    print('array after:', array)

# if-elif-else用法
```python
age=int(input('请输入年龄'))
sex=input('请输入性别')
if age>=19 and sex=='男':
    print ('该上班了')
elif age<18 or sex=='女':
    print('上学吧还是')
elif not(sex=='男' and  sex=='女'):
    print('既不是男也不是女')
else:
    pass#以后要填充代码的，为了保证不会出现语法错误
```

# 本机ip
```python
# https://github.com/striver-ing/wechat-spider/blob/8ec61fa16f10abcb027ed8a7fde19177e9d1b5c1/wechat-spider/config.py#L26-L45

def get_host_ip():
    """
    利用 UDP 协议来实现的，生成一个UDP包，把自己的 IP 放如到 UDP 协议头中，然后从UDP包中获取本机的IP。
    这个方法并不会真实的向外部发包，所以用抓包工具是看不到的
    :return:
    """
    s = None
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        if s:
            s.close()

    return ip


IP = get_host_ip()

```

## Python：os 取分隔符
os 模块属性	描述
linesep	用于在文件中分隔行的字符串
sep	用来分隔文件路径名的字符串
pathsep	用于分隔文件路径的字符串
curdir	当前工作目录的字符串名称
pardir	(当前工作目录的)父目录字符串名称