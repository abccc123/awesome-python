

## 开发工具
* [./PyCharm 2019.2.3.txt](PyCharm 2019.2.3.txt)
* [readwithu](https://www.readwithu.com/Article/supplement/Python%E5%85%B3%E9%94%AE%E5%AD%97yield.html)
* [python3](https://python3-cookbook.readthedocs.io/zh_CN/latest/)
* []()
* []()

## 资料
* [面试](https://github.com/imtiantian/Python_Awesome_Interview)
* [python-mini-projects](https://github.com/basicv8vc/Python-Machine-Learning-zh)
* [](https://github.com/chavarera/python-mini-projects/blob/master/projects/Dns_record/dns_record.py)
* []()
* []()
* []()



## 6种打包Python代码的方法
pyinstaller、cx_Freeze、py2exe、py2app、Nuitka和Nuitka+pyinstaller。
每种方式都有其优点和缺点，开发者可以根据自己的需求选择合适的方式
一、pyinstaller
简介：
PyInstaller 是一个用于将 Python 程序打包成独立可执行文件的工具，支持 Windows、Linux 和 macOS 等多个平台。
特点：
使用相对简单，通过命令行操作即可完成打包过程。
可以打包单个脚本或者整个 Python 项目，包括依赖的第三方库。
支持多种打包模式，如生成单个可执行文件或包含多个文件的目录结构。
优势：
广泛的平台支持使得打包后的程序可以在不同操作系统上运行，无需安装 Python 解释器。
社区活跃，遇到问题容易找到解决方案。
二、cx_Freeze
简介：
cx_Freeze 也是一个将 Python 脚本转换为可执行文件的工具，同样适用于多个操作系统。
特点：
可以通过编写 setup.py 文件进行配置，灵活性较高。
支持打包为多种格式，如 Windows 下的 exe 文件、Linux 下的可执行文件等。
优势：
对于复杂项目的打包有较好的支持，可以处理一些特殊的依赖情况。
可以与其他 Python 工具和库配合使用，方便进行项目的构建和部署。
三、py2exe
简介：
主要用于将 Python 脚本打包成 Windows 平台下的可执行文件。
特点：
专注于 Windows 平台，对该平台的特性有较好的支持。
可以通过编写 setup.py 文件进行详细的配置。
优势：
在 Windows 环境下打包效果较好，对于一些特定的 Windows 应用场景有优势。
四、py2app
简介：
专门用于将 Python 程序打包成 macOS 平台下的应用程序。
特点：
与 macOS 的开发环境和工具链集成较好。
可以生成具有 macOS 风格的应用程序，包括图标、菜单栏等。
优势：
对于在 macOS 平台上发布 Python 应用非常方便。
五、Nuitka
简介：
Nuitka 是一个将 Python 代码编译为 C 代码的工具，从而提高程序的执行速度，并可以生成独立的可执行文件。
特点：
不仅可以打包程序，还能对代码进行优化，提高性能。
支持多种 Python 版本和操作系统。
优势：
生成的可执行文件执行速度快，尤其对于大型项目或对性能要求较高的应用有明显优势。
提供了丰富的编译选项，可以根据具体需求进行定制。